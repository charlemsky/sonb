package com;

import com.data.*;
import org.junit.Test;

import java.util.*;

public class NodeManagerTest {

    private UUID prepareNodeServer(State state) {
        UUID uuid = UUID.randomUUID();
        ServerNode serverNode = new ServerNode()
                .withEnabled(true)
                .withStates(state)
                .withUuid(uuid);
        NodeManager.getInstance().addNode(serverNode);
        return uuid;
    }

    private List<UUID>  setUpServer() {
        List<UUID> nodeList = new ArrayList<>();
        nodeList.add(prepareNodeServer(new State(new HashSet<>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<>(Arrays.asList("1","2","3")))));
        return nodeList;
    }

    @Test
    public void serverTest() {
        List<UUID> nodeList = setUpServer();
        NodeManager.getInstance().getNode(nodeList.get(0)).modify(
                MessageData.newBuilder()
                        .withContent("1")
                        .withType(MessageType.DELETE)
                        .build()
        );


    }








}
