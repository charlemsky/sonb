package com.data;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
public class ServerNode {

    private State nodeState;

    private boolean enabled;

    private UUID uuid;

    private void fireModifyStateEvent(UUID uuid) {
        log.info("fireModifyStateEvent|| {}", uuid);
        Thread thread = new Thread() {
            @Override
            public void run() {
                NodeManager.getInstance().fireSynchronizationEvent(uuid);

            }
        };
        thread.start();
    }

    public void modify(MessageData messageData) {
        log.info("modify:start|| " + uuid + " message data: " + messageData.getContent());
        Thread thread = new Thread() {
            @Override
            public void run() {
                boolean canModify = false;

                while (!canModify) {
                    try {
                        Thread.sleep(1000L);
                        log.info("wait to modify...");
                        canModify = NodeManager.getInstance().checkActualData();
                    } catch (InterruptedException e) {
                        log.error(e.getMessage());
                    }

                }

                modifyState(messageData);
                fireModifyStateEvent(uuid);
                log.info("modify:stop|| " + uuid + " message data: " + messageData.getContent());
            }
        };
        thread.start();
    }


    void synchronize(State state) {
        log.info("try synchronize||" + uuid + "||" + state.getContentSet());
        if (this.isEnabled() && state.getSyncDate().compareTo(this.nodeState.getSyncDate()) > 0) {
            log.info("SYNCHRONIZE!! NODE||" + uuid + "||" + state.getContentSet());
            StateUtil.modifyState(this, state);
            fireModifyStateEvent(uuid);

        }
    }

    public State getNodeState() {
        return nodeState;
    }

    public ServerNode withStates(State states) {
        this.nodeState = states;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public ServerNode withEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(boolean enabled) {
        if(enabled) {
            nodeState = new State(NodeManager.getInstance().getActualDate());
        }
        this.enabled = enabled;

    }

    public UUID getUuid() {
        return uuid;
    }

    public ServerNode withUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }


    private void modifyState(MessageData messageData) {
        switch (messageData.getType()) {
            case CREATE:
                nodeState.add(messageData.getContent());
                break;
            case DELETE:
                nodeState.remove(messageData.getContent());
                break;
        }
    }
}
