package com.data;

import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
public class NodeManager {


    private static volatile NodeManager instance = null;


    private Set<ServerNode> registry = new HashSet<ServerNode>();

    void fireSynchronizationEvent(UUID uuid) {
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("synchronizeNodes|| {}" , uuid);
        synchronizeNodes(getNode(uuid));
    }

    private void synchronizeNodes(ServerNode serverNode) {
        log.info("synchronizeNodes|| {}" , serverNode.getUuid());
        registry.forEach( r ->  r.synchronize(serverNode.getNodeState()));
    }

    private NodeManager() {
    }

    public static NodeManager getInstance() {
        if (instance == null) {
            synchronized (NodeManager.class) {
                if (instance == null) {
                    instance = new NodeManager();
                }
            }
        }
        return instance;
    }

    public void addNode(ServerNode node) {
        log.info("addNode|| {}" , node.getUuid());
        registry.add(node);
    }

    public ServerNode getNode(final UUID uuid) {
        log.info("getNode|| {}" , uuid);
        return registry.stream()
                .filter(e -> e.getUuid().equals(uuid))
                .findFirst()
                .orElse(null);
    }

    public boolean checkActualData() {
        final Set<ServerNode> enabledServer = NodeManager.getInstance()
                .registry
                .stream()
                .filter(ServerNode::isEnabled)
                .collect(Collectors.toSet());

        for (ServerNode outer: enabledServer) {
            for (ServerNode inner: enabledServer) {
                if(!inner.getNodeState().getContentSet().equals(outer.getNodeState().getContentSet())) {
                    return false;
                }
            }
        }
        return true;
    }

    public Set<String> getActualDate() {
        final Set<ServerNode> enabledServer = NodeManager.getInstance()
                .registry
                .stream()
                .filter(ServerNode::isEnabled)
                .collect(Collectors.toSet());
        final State data  = enabledServer.stream().findAny().get().getNodeState();
        Set<String> dataSet = data.getContentSet();
        for (ServerNode serverNode: enabledServer) {
            if (serverNode.getNodeState().getSyncDate().after(data.getSyncDate())) {
               dataSet = serverNode.getNodeState().getContentSet();
            }
        }
        return dataSet;
    }



}
