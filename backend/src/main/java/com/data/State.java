package com.data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class State implements Serializable {

    private Set<String> contentSet = new HashSet<>();

    private Date syncDate;

    public Set<String> getContentSet() {
        return contentSet;
    }

    public State(Set<String> contentSet) {
        this.contentSet = contentSet;
        syncDate = new Date();
    }

    public void setContentSet(Set<String> contentSet) {
        this.contentSet = contentSet;
    }

    public Date getSyncDate() {
        return syncDate;
    }



    public void remove(String content) {
        this.syncDate = new Date();
        contentSet.remove(content);
    }

    public void add(String content) {
        this.syncDate = new Date();
        contentSet.add(content);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        return contentSet != null ? contentSet.equals(state.contentSet) : state.contentSet == null;
    }

    @Override
    public int hashCode() {
        return contentSet != null ? contentSet.hashCode() : 0;
    }

    @Override
    public String toString() {
        return  contentSet.toString();
    }
}
