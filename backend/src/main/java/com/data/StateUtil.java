package com.data;

import java.util.HashSet;

public class StateUtil {

    public static void modifyState(ServerNode serverNode, State state) {
        if(!serverNode.getNodeState().equals(state) ){
            merge(serverNode.getNodeState(), state);
        }
    }
    public static void merge(State state, State state2) {
        state.setContentSet(new HashSet<>(state2.getContentSet()));
    }
}
