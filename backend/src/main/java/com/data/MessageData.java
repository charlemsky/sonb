package com.data;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Getter
public class MessageData implements Serializable {

    private MessageType type;

    private String content;

    private MessageData(MessageType type, String content) {
        this.type = type;
        this.content = content;
    }
    public static Builder newBuilder() {
        return new Builder();
    }


    public static class Builder {
        private MessageType type;
        private String content;

        public Builder withType(MessageType type) {
            this.type = type;
            return this;
        }

        public Builder withContent(String content) {
            this.content = content;
            return this;
        }

        public MessageData build() {
            return new MessageData(type, content);
        }

    }
}
