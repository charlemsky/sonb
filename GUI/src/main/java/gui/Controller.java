package gui;



import com.data.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.*;


public class Controller {


    public Button modify1;
    public Button enable1;
    public Button save1;
    public TextField state1;
    public Button modify2;
    public Button enable2;
    public Button save2;
    public TextField state2;
    public Button modify3;
    public Button save3;
    public Button enable3;
    public TextField state3;
    public Button modify4;
    public Button enable4;
    public Button save4;
    public TextField state4;
    public Button modify5;
    public Button enable5;
    public Button save5;
    public TextField state5;
    public Button modify6;
    public Button enable6;
    public Button save6;
    public TextField state6;
    public TextField message6;
    public TextField message5;
    public TextField message4;
    public TextField message3;
    public TextField message2;
    public TextField message1;
    public Label nodeState1;
    public Label nodeState2;
    public Label nodeState3;
    public Label nodeState4;
    public Label nodeState5;
    public Label nodeState6;
    public static final String ENABLE = "ENABLE";
    public static final String DISABLE = "DISABLE";
    private List<UUID> nodeIdList;

    private UUID prepareNodeServer(State state) {
        UUID uuid = UUID.randomUUID();
        ServerNode serverNode = new ServerNode()
                .withEnabled(true)
                .withStates(state)
                .withUuid(uuid);
        NodeManager.getInstance().addNode(serverNode);
        return uuid;
    }

    private List<UUID> setUpServer() {
        List<UUID> nodeList = new ArrayList<UUID>();
        nodeList.add(prepareNodeServer(new State(new HashSet<String>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<String>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<String>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<String>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<String>(Arrays.asList("1","2")))));
        nodeList.add(prepareNodeServer(new State(new HashSet<String>(Arrays.asList("1","2")))));
        return nodeList;
    }

    @FXML
    public void initialize() {
        nodeIdList = setUpServer();

        updateField();

        state1.setDisable(true);
        state2.setDisable(true);
        state3.setDisable(true);
        state4.setDisable(true);
        state5.setDisable(true);
        state6.setDisable(true);

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            updateField();
                                           }
                    });

                }
            }
        });
        t1.start();
    }

    private void updateField() {
        state1.setText(NodeManager.getInstance().getNode(nodeIdList.get(0)).getNodeState().toString());
        state2.setText(NodeManager.getInstance().getNode(nodeIdList.get(1)).getNodeState().toString());
        state3.setText(NodeManager.getInstance().getNode(nodeIdList.get(2)).getNodeState().toString());
        state4.setText(NodeManager.getInstance().getNode(nodeIdList.get(3)).getNodeState().toString());
        state5.setText(NodeManager.getInstance().getNode(nodeIdList.get(4)).getNodeState().toString());
        state6.setText(NodeManager.getInstance().getNode(nodeIdList.get(5)).getNodeState().toString());
    }


    public void modifyAction(ActionEvent actionEvent) {

        String field1 = ((Button) actionEvent.getSource()).getId();
        switch (field1){
            case "modify1" :
                modifyState(0, message1);
                break;
            case "modify2" :
                modifyState(1, message2);
                 break;
            case "modify3" :
                modifyState(2, message3);
                break;
            case "modify4" :
                modifyState(3, message4);
                break;
            case "modify5" :
                modifyState(4, message5);
                break;
            case "modify6" :
                modifyState(5, message6);
                break;
            default: break;
        }
    }

    private void modifyState(final int nodeId, final TextField field) {
        Thread t1 = new Thread(new Runnable() {
            public void run() {

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            NodeManager.getInstance()
                                    .getNode(nodeIdList.get(nodeId))
                                    .modify(MessageData.newBuilder()
                                            .withType(field.getText().contains("-") ? MessageType.DELETE : MessageType.CREATE)
                                            .withContent(field.getText().replace("-",""))
                                            .build());                        }
                    });


            }
        });
        t1.start();

    }

    private void modifyEnabled(final int nodeId, final boolean enable) {
        Thread t1 = new Thread(new Runnable() {
            public void run() {

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        NodeManager.getInstance()
                                .getNode(nodeIdList.get(nodeId))
                                .setEnabled(enable);
                    }
                });


            }
        });
        t1.start();

    }


    public void enableAction(ActionEvent actionEvent) {

        String field1 = ((Button) actionEvent.getSource()).getId();
        switch (field1){
            case "enable1" : modifyEnabled(0, updateNodeState(nodeState1)); break;
            case "enable2" : modifyEnabled(1, updateNodeState(nodeState2)); break;
            case "enable3" : modifyEnabled(2, updateNodeState(nodeState3)); break;
            case "enable4" : modifyEnabled(3, updateNodeState(nodeState4)); break;
            case "enable5" : modifyEnabled(4, updateNodeState(nodeState5)); break;
            case "enable6" : modifyEnabled(5, updateNodeState(nodeState6)); break;
            default: break;
        }
    }

    private boolean updateNodeState(final Label label) {
        final boolean actualValue = label.getText().equals(ENABLE);
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (actualValue) {
                            label.setText(DISABLE);
                        } else {
                            label.setText(ENABLE);
                        }
                    }
                });
            }
        });
        t1.start();
        return !actualValue;
    }

}
